package com.samip.mydemo3_matrimony.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.samip.mydemo3_matrimony.R;
import com.samip.mydemo3_matrimony.adapter.CityAdapter;
import com.samip.mydemo3_matrimony.adapter.LanguageAdapter;
import com.samip.mydemo3_matrimony.database.TblMstCity;
import com.samip.mydemo3_matrimony.database.TblMstLanguage;
import com.samip.mydemo3_matrimony.database.TblUser;
import com.samip.mydemo3_matrimony.fragment.UserListFragment;
import com.samip.mydemo3_matrimony.model.CityModel;
import com.samip.mydemo3_matrimony.model.LanguageModel;
import com.samip.mydemo3_matrimony.model.UserModel;
import com.samip.mydemo3_matrimony.util.Constant;
import com.samip.mydemo3_matrimony.util.Utils;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUserActivity extends BaseActivity  implements DatePickerDialog.OnDateSetListener{
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.etFatherName)
    TextInputEditText etFatherName;
    @BindView(R.id.etSurName)
    TextInputEditText etSurName;
    @BindView(R.id.rbMale)
    MaterialRadioButton rbMale;
    @BindView(R.id.rbFeMale)
    MaterialRadioButton rbFeMale;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.etDob)
    TextInputEditText etDob;
    @BindView(R.id.etPhoneNumber)
    TextInputEditText etPhoneNumber;
    @BindView(R.id.etEmailAddress)
    TextInputEditText etEmailAddress;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.screen_Layout)
    FrameLayout screenLayout;

    String startingDate = "1990-08-10";
    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;

    ArrayList<CityModel> cityList = new ArrayList<>();
    ArrayList<LanguageModel> languageList = new ArrayList<>();

    UserModel userModel;

    int citySelected,languageSelected;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_add_user),true);
        setDataToView();
        setSpinnerAdapter();
        getDataForUpdate();
    }

    int getSelectedPositionFromCityId(int cityId) {
        for (int i = 0; i < cityList.size(); i++) {
            if (cityList.get(i).getCityID() == cityId) {
                return i;
            }
        }
        return 0;
    }

    int getSelectedPositionFromLanguageId(int languageId) {
        for (int i = 0; i < languageList.size(); i++) {
            if (languageList.get(i).getLanguageID() == languageId) {
                return i;
            }
        }
        return 0;
    }
    void setSpinnerAdapter() {
        cityList.addAll(new TblMstCity(this).getCityList());
        languageList.addAll(new TblMstLanguage(this).getLanguages());

        cityAdapter = new CityAdapter(this, cityList);
        languageAdapter = new LanguageAdapter(this, languageList);

        spCity.setAdapter(cityAdapter);
        spLanguage.setAdapter(languageAdapter);

        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                citySelected = cityList.get(i).getCityID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                languageSelected = languageList.get(i).getLanguageID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    void setDataToView() {
        final Calendar newCalendar = Calendar.getInstance();
        etDob.setText(
                String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                        String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/" +
                        String.format("%02d", newCalendar.get(Calendar.YEAR)));
    }

    void getDataForUpdate(){
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            getSupportActionBar().setTitle(R.string.lbl_edit_user);
            etName.setText(userModel.getName());
            etFatherName.setText(userModel.getFatherName());
            etSurName.setText(userModel.getSurname());
            etPhoneNumber.setText(userModel.getPhoneNumber());
            etDob.setText(userModel.getDob());
            if (userModel.getGender() == Constant.MALE) {
                rbMale.setChecked(true);
            } else {
                rbFeMale.setChecked(true);
            }
            etEmailAddress.setText(userModel.getEmail());

            spCity.setSelection(getSelectedPositionFromCityId(userModel.getCityID()));
            spLanguage.setSelection(getSelectedPositionFromLanguageId(userModel.getLanguageID()));
        }
    }

    @OnClick(R.id.etDob)
    public void onEtDobClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        Date date = Utils.getDateFromString(startingDate);
        newCalendar.setTimeInMillis(date.getTime());
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                AddUserActivity.this, (datePicker, year, month, day) -> etDob.setText(String.format("%02d", day) + "/" + String.format("%02d", (month + 1)) + "/" + year),
                newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
    @OnClick(R.id.btnSubmit)
    public void onBtnSubmitClicked() {
        if (isValidUser()) {
            if (userModel == null) {
                long lastInsetedID = new TblUser(getApplicationContext()).insertUser(etName.getText().toString(),
                        etFatherName.getText().toString(), etSurName.getText().toString(), rbMale.isChecked() ? Constant.MALE : Constant.FEMALE,
                        "", Utils.getFormatedDateToInsert(etDob.getText().toString()),
                        etPhoneNumber.getText().toString(),
                        etEmailAddress.getText().toString(),
                        languageSelected,
                        citySelected,0);
                showToast(lastInsetedID > 0 ? "User Inserted Successfully" : "Something went wrong");

                showToast("C : "+citySelected + "  L : " + languageSelected);
            }
            else{
                long lastInsetedID = new TblUser(getApplicationContext()).updateUserById(etName.getText().toString(),
                        etFatherName.getText().toString(), etSurName.getText().toString(), rbMale.isChecked() ? Constant.MALE : Constant.FEMALE,
                        "", Utils.getFormatedDateToInsert(etDob.getText().toString()),
                        etPhoneNumber.getText().toString(),
                        etEmailAddress.getText().toString(),
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguageID(),
                        cityList.get(spCity.getSelectedItemPosition()).getCityID(),userModel.getIsFavorite(),userModel.getUserId());
                showToast(lastInsetedID > 0 ? "User Updated Successfully" : "Something went wrong");

            }
            Intent intent = new Intent(AddUserActivity.this,ActivityUserListByGender.class);
            startActivity(intent);
            finish();
        }
    }


    @Override
public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }



    boolean isValidUser() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etName.getText().toString())) {
            isValid = false;
            etName.setError(getString(R.string.lbl_name));
        }

        if (TextUtils.isEmpty(etFatherName.getText().toString())) {
            isValid = false;
            etFatherName.setError(getString(R.string.error_father_name));
        }

        if (TextUtils.isEmpty(etSurName.getText().toString())) {
            isValid = false;
            etSurName.setError(getString(R.string.error_enter_surname));
        }

        if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_enter_phone));
        } else if (etPhoneNumber.getText().toString().length() < 10) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_valid_phone));
        }

        if (TextUtils.isEmpty(etEmailAddress.getText().toString())) {
            isValid = false;
            etEmailAddress.setError(getString(R.string.error_ente_email));
        } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmailAddress.getText().toString()).matches()) {
            isValid = false;
            etEmailAddress.setError(getString(R.string.error_valid_email));
        }

        if (spCity.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_city_select));
        }

        if (spLanguage.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_language_select));
        }
        return isValid;
    }
}
