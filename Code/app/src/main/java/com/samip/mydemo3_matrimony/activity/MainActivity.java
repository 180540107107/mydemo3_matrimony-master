package com.samip.mydemo3_matrimony.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.samip.mydemo3_matrimony.R;
import com.samip.mydemo3_matrimony.database.MyDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.samip.mydemo3_matrimony.R.*;

public class MainActivity extends BaseActivity {


    @BindView(id.toolbar)
    Toolbar toolbar;
    @BindView(id.cvActRegistration)
    CardView cvActRegistration;
    @BindView(id.cvActList)
    CardView cvActList;
    @BindView(id.cvActFavotire)
    CardView cvActFavotire;
    @BindView(id.cvActSearch)
    CardView cvActSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_main);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.lbl_dashboard),false);
    }


    @OnClick(id.cvActRegistration)
    public void onCvActRegistrationClicked() {
        Intent intent = new Intent(this,AddUserActivity.class);
        startActivity(intent);
    }

    @OnClick(id.cvActList)
    public void onCvActListClicked() {
        Intent intent = new Intent(this,ActivityUserListByGender.class);
        startActivity(intent);
    }

    @OnClick(id.cvActFavotire)
    public void onCvActFavotireClicked() {
        Intent intent = new Intent(this,UserListActivity.class);
        startActivity(intent);
    }

    @OnClick(id.cvActSearch)
    public void onCvActSearchClicked() {
        Intent intent = new Intent(this,ActivitySearchUser.class);
        startActivity(intent);
    }
}